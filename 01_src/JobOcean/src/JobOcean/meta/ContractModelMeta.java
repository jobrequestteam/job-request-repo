package JobOcean.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2016-10-16 16:49:14")
/** */
public final class ContractModelMeta extends org.slim3.datastore.ModelMeta<JobOcean.model.ContractModel> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, java.util.Date> closedDate = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, java.util.Date>(this, "closedDate", "closedDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<JobOcean.model.ContractModel> contractNumber = new org.slim3.datastore.StringAttributeMeta<JobOcean.model.ContractModel>(this, "contractNumber", "contractNumber");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<JobOcean.model.ContractModel> contractorName = new org.slim3.datastore.StringAttributeMeta<JobOcean.model.ContractModel>(this, "contractorName", "contractorName");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, java.util.Date> creationDate = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, java.util.Date>(this, "creationDate", "creationDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, java.util.Date> endDate = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, java.util.Date>(this, "endDate", "endDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, java.lang.Long> id = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, java.lang.Long>(this, "id", "id", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, java.util.Date> modificationDate = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, java.util.Date>(this, "modificationDate", "modificationDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, java.util.Date> startDate = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, java.util.Date>(this, "startDate", "startDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<JobOcean.model.ContractModel> status = new org.slim3.datastore.StringAttributeMeta<JobOcean.model.ContractModel>(this, "status", "status");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<JobOcean.model.ContractModel> type = new org.slim3.datastore.StringAttributeMeta<JobOcean.model.ContractModel>(this, "type", "type");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.ContractModel, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final org.slim3.datastore.ModificationDate slim3_creationDateAttributeListener = new org.slim3.datastore.ModificationDate();

    private static final org.slim3.datastore.CreationDate slim3_modificationDateAttributeListener = new org.slim3.datastore.CreationDate();

    private static final ContractModelMeta slim3_singleton = new ContractModelMeta();

    /**
     * @return the singleton
     */
    public static ContractModelMeta get() {
       return slim3_singleton;
    }

    /** */
    public ContractModelMeta() {
        super("ContractModel", JobOcean.model.ContractModel.class);
    }

    @Override
    public JobOcean.model.ContractModel entityToModel(com.google.appengine.api.datastore.Entity entity) {
        JobOcean.model.ContractModel model = new JobOcean.model.ContractModel();
        model.setClosedDate((java.util.Date) entity.getProperty("closedDate"));
        model.setContractNumber((java.lang.String) entity.getProperty("contractNumber"));
        model.setContractorName((java.lang.String) entity.getProperty("contractorName"));
        model.setCreationDate((java.util.Date) entity.getProperty("creationDate"));
        model.setEndDate((java.util.Date) entity.getProperty("endDate"));
        model.setId((java.lang.Long) entity.getProperty("id"));
        model.setKey(entity.getKey());
        model.setModificationDate((java.util.Date) entity.getProperty("modificationDate"));
        model.setStartDate((java.util.Date) entity.getProperty("startDate"));
        model.setStatus((java.lang.String) entity.getProperty("status"));
        model.setType((java.lang.String) entity.getProperty("type"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        JobOcean.model.ContractModel m = (JobOcean.model.ContractModel) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("closedDate", m.getClosedDate());
        entity.setProperty("contractNumber", m.getContractNumber());
        entity.setProperty("contractorName", m.getContractorName());
        entity.setProperty("creationDate", m.getCreationDate());
        entity.setProperty("endDate", m.getEndDate());
        entity.setProperty("id", m.getId());
        entity.setProperty("modificationDate", m.getModificationDate());
        entity.setProperty("startDate", m.getStartDate());
        entity.setProperty("status", m.getStatus());
        entity.setProperty("type", m.getType());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        JobOcean.model.ContractModel m = (JobOcean.model.ContractModel) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        JobOcean.model.ContractModel m = (JobOcean.model.ContractModel) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        JobOcean.model.ContractModel m = (JobOcean.model.ContractModel) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        JobOcean.model.ContractModel m = (JobOcean.model.ContractModel) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
        JobOcean.model.ContractModel m = (JobOcean.model.ContractModel) model;
        m.setCreationDate(slim3_creationDateAttributeListener.prePut(m.getCreationDate()));
        m.setModificationDate(slim3_modificationDateAttributeListener.prePut(m.getModificationDate()));
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        JobOcean.model.ContractModel m = (JobOcean.model.ContractModel) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getClosedDate() != null){
            writer.setNextPropertyName("closedDate");
            encoder0.encode(writer, m.getClosedDate());
        }
        if(m.getContractNumber() != null){
            writer.setNextPropertyName("contractNumber");
            encoder0.encode(writer, m.getContractNumber());
        }
        if(m.getContractorName() != null){
            writer.setNextPropertyName("contractorName");
            encoder0.encode(writer, m.getContractorName());
        }
        if(m.getCreationDate() != null){
            writer.setNextPropertyName("creationDate");
            encoder0.encode(writer, m.getCreationDate());
        }
        if(m.getEndDate() != null){
            writer.setNextPropertyName("endDate");
            encoder0.encode(writer, m.getEndDate());
        }
        if(m.getId() != null){
            writer.setNextPropertyName("id");
            encoder0.encode(writer, m.getId());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getModificationDate() != null){
            writer.setNextPropertyName("modificationDate");
            encoder0.encode(writer, m.getModificationDate());
        }
        if(m.getStartDate() != null){
            writer.setNextPropertyName("startDate");
            encoder0.encode(writer, m.getStartDate());
        }
        if(m.getStatus() != null){
            writer.setNextPropertyName("status");
            encoder0.encode(writer, m.getStatus());
        }
        if(m.getType() != null){
            writer.setNextPropertyName("type");
            encoder0.encode(writer, m.getType());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected JobOcean.model.ContractModel jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        JobOcean.model.ContractModel m = new JobOcean.model.ContractModel();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("closedDate");
        m.setClosedDate(decoder0.decode(reader, m.getClosedDate()));
        reader = rootReader.newObjectReader("contractNumber");
        m.setContractNumber(decoder0.decode(reader, m.getContractNumber()));
        reader = rootReader.newObjectReader("contractorName");
        m.setContractorName(decoder0.decode(reader, m.getContractorName()));
        reader = rootReader.newObjectReader("creationDate");
        m.setCreationDate(decoder0.decode(reader, m.getCreationDate()));
        reader = rootReader.newObjectReader("endDate");
        m.setEndDate(decoder0.decode(reader, m.getEndDate()));
        reader = rootReader.newObjectReader("id");
        m.setId(decoder0.decode(reader, m.getId()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("modificationDate");
        m.setModificationDate(decoder0.decode(reader, m.getModificationDate()));
        reader = rootReader.newObjectReader("startDate");
        m.setStartDate(decoder0.decode(reader, m.getStartDate()));
        reader = rootReader.newObjectReader("status");
        m.setStatus(decoder0.decode(reader, m.getStatus()));
        reader = rootReader.newObjectReader("type");
        m.setType(decoder0.decode(reader, m.getType()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}