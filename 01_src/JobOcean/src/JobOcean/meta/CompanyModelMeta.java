package JobOcean.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2016-10-16 16:49:14")
/** */
public final class CompanyModelMeta extends org.slim3.datastore.ModelMeta<JobOcean.model.CompanyModel> {

    /** */
    public final org.slim3.datastore.StringAttributeMeta<JobOcean.model.CompanyModel> address = new org.slim3.datastore.StringAttributeMeta<JobOcean.model.CompanyModel>(this, "address", "address");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<JobOcean.model.CompanyModel> companyName = new org.slim3.datastore.StringAttributeMeta<JobOcean.model.CompanyModel>(this, "companyName", "companyName");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<JobOcean.model.CompanyModel> companyUrl = new org.slim3.datastore.StringAttributeMeta<JobOcean.model.CompanyModel>(this, "companyUrl", "companyUrl");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.CompanyModel, java.util.Date> creationDate = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.CompanyModel, java.util.Date>(this, "creationDate", "creationDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<JobOcean.model.CompanyModel> email = new org.slim3.datastore.StringAttributeMeta<JobOcean.model.CompanyModel>(this, "email", "email");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.CompanyModel, java.lang.Long> id = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.CompanyModel, java.lang.Long>(this, "id", "id", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.CompanyModel, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.CompanyModel, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.CompanyModel, java.util.Date> modificationDate = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.CompanyModel, java.util.Date>(this, "modificationDate", "modificationDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<JobOcean.model.CompanyModel> password = new org.slim3.datastore.StringAttributeMeta<JobOcean.model.CompanyModel>(this, "password", "password");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<JobOcean.model.CompanyModel> telNumber = new org.slim3.datastore.StringAttributeMeta<JobOcean.model.CompanyModel>(this, "telNumber", "telNumber");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.CompanyModel, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.CompanyModel, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final org.slim3.datastore.CreationDate slim3_creationDateAttributeListener = new org.slim3.datastore.CreationDate();

    private static final org.slim3.datastore.ModificationDate slim3_modificationDateAttributeListener = new org.slim3.datastore.ModificationDate();

    private static final CompanyModelMeta slim3_singleton = new CompanyModelMeta();

    /**
     * @return the singleton
     */
    public static CompanyModelMeta get() {
       return slim3_singleton;
    }

    /** */
    public CompanyModelMeta() {
        super("CompanyModel", JobOcean.model.CompanyModel.class);
    }

    @Override
    public JobOcean.model.CompanyModel entityToModel(com.google.appengine.api.datastore.Entity entity) {
        JobOcean.model.CompanyModel model = new JobOcean.model.CompanyModel();
        model.setAddress((java.lang.String) entity.getProperty("address"));
        model.setCompanyName((java.lang.String) entity.getProperty("companyName"));
        model.setCompanyUrl((java.lang.String) entity.getProperty("companyUrl"));
        model.setCreationDate((java.util.Date) entity.getProperty("creationDate"));
        model.setEmail((java.lang.String) entity.getProperty("email"));
        model.setId((java.lang.Long) entity.getProperty("id"));
        model.setKey(entity.getKey());
        model.setModificationDate((java.util.Date) entity.getProperty("modificationDate"));
        model.setPassword((java.lang.String) entity.getProperty("password"));
        model.setTelNumber((java.lang.String) entity.getProperty("telNumber"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        JobOcean.model.CompanyModel m = (JobOcean.model.CompanyModel) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("address", m.getAddress());
        entity.setProperty("companyName", m.getCompanyName());
        entity.setProperty("companyUrl", m.getCompanyUrl());
        entity.setProperty("creationDate", m.getCreationDate());
        entity.setProperty("email", m.getEmail());
        entity.setProperty("id", m.getId());
        entity.setProperty("modificationDate", m.getModificationDate());
        entity.setProperty("password", m.getPassword());
        entity.setProperty("telNumber", m.getTelNumber());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        JobOcean.model.CompanyModel m = (JobOcean.model.CompanyModel) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        JobOcean.model.CompanyModel m = (JobOcean.model.CompanyModel) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        JobOcean.model.CompanyModel m = (JobOcean.model.CompanyModel) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        JobOcean.model.CompanyModel m = (JobOcean.model.CompanyModel) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
        JobOcean.model.CompanyModel m = (JobOcean.model.CompanyModel) model;
        m.setCreationDate(slim3_creationDateAttributeListener.prePut(m.getCreationDate()));
        m.setModificationDate(slim3_modificationDateAttributeListener.prePut(m.getModificationDate()));
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        JobOcean.model.CompanyModel m = (JobOcean.model.CompanyModel) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getAddress() != null){
            writer.setNextPropertyName("address");
            encoder0.encode(writer, m.getAddress());
        }
        if(m.getCompanyName() != null){
            writer.setNextPropertyName("companyName");
            encoder0.encode(writer, m.getCompanyName());
        }
        if(m.getCompanyUrl() != null){
            writer.setNextPropertyName("companyUrl");
            encoder0.encode(writer, m.getCompanyUrl());
        }
        if(m.getCreationDate() != null){
            writer.setNextPropertyName("creationDate");
            encoder0.encode(writer, m.getCreationDate());
        }
        if(m.getEmail() != null){
            writer.setNextPropertyName("email");
            encoder0.encode(writer, m.getEmail());
        }
        if(m.getId() != null){
            writer.setNextPropertyName("id");
            encoder0.encode(writer, m.getId());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getModificationDate() != null){
            writer.setNextPropertyName("modificationDate");
            encoder0.encode(writer, m.getModificationDate());
        }
        if(m.getPassword() != null){
            writer.setNextPropertyName("password");
            encoder0.encode(writer, m.getPassword());
        }
        if(m.getTelNumber() != null){
            writer.setNextPropertyName("telNumber");
            encoder0.encode(writer, m.getTelNumber());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected JobOcean.model.CompanyModel jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        JobOcean.model.CompanyModel m = new JobOcean.model.CompanyModel();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("address");
        m.setAddress(decoder0.decode(reader, m.getAddress()));
        reader = rootReader.newObjectReader("companyName");
        m.setCompanyName(decoder0.decode(reader, m.getCompanyName()));
        reader = rootReader.newObjectReader("companyUrl");
        m.setCompanyUrl(decoder0.decode(reader, m.getCompanyUrl()));
        reader = rootReader.newObjectReader("creationDate");
        m.setCreationDate(decoder0.decode(reader, m.getCreationDate()));
        reader = rootReader.newObjectReader("email");
        m.setEmail(decoder0.decode(reader, m.getEmail()));
        reader = rootReader.newObjectReader("id");
        m.setId(decoder0.decode(reader, m.getId()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("modificationDate");
        m.setModificationDate(decoder0.decode(reader, m.getModificationDate()));
        reader = rootReader.newObjectReader("password");
        m.setPassword(decoder0.decode(reader, m.getPassword()));
        reader = rootReader.newObjectReader("telNumber");
        m.setTelNumber(decoder0.decode(reader, m.getTelNumber()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}