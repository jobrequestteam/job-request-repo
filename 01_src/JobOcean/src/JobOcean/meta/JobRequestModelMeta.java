package JobOcean.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2016-10-16 16:49:14")
/** */
public final class JobRequestModelMeta extends org.slim3.datastore.ModelMeta<JobOcean.model.JobRequestModel> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.util.Date> closedDate = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.util.Date>(this, "closedDate", "closedDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.lang.Long> companyId = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.lang.Long>(this, "companyId", "companyId", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.StringCollectionAttributeMeta<JobOcean.model.JobRequestModel, java.util.ArrayList<java.lang.String>> contracts = new org.slim3.datastore.StringCollectionAttributeMeta<JobOcean.model.JobRequestModel, java.util.ArrayList<java.lang.String>>(this, "contracts", "contracts", java.util.ArrayList.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.util.Date> creationDate = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.util.Date>(this, "creationDate", "creationDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<JobOcean.model.JobRequestModel> description = new org.slim3.datastore.StringAttributeMeta<JobOcean.model.JobRequestModel>(this, "description", "description");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.util.Date> endDate = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.util.Date>(this, "endDate", "endDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.lang.Long> id = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.lang.Long>(this, "id", "id", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<JobOcean.model.JobRequestModel> jobRequestNumber = new org.slim3.datastore.StringAttributeMeta<JobOcean.model.JobRequestModel>(this, "jobRequestNumber", "jobRequestNumber");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.util.Date> modificationDate = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.util.Date>(this, "modificationDate", "modificationDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.util.Date> startDate = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.util.Date>(this, "startDate", "startDate", java.util.Date.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<JobOcean.model.JobRequestModel> status = new org.slim3.datastore.StringAttributeMeta<JobOcean.model.JobRequestModel>(this, "status", "status");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<JobOcean.model.JobRequestModel, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final org.slim3.datastore.ModificationDate slim3_creationDateAttributeListener = new org.slim3.datastore.ModificationDate();

    private static final org.slim3.datastore.CreationDate slim3_modificationDateAttributeListener = new org.slim3.datastore.CreationDate();

    private static final JobRequestModelMeta slim3_singleton = new JobRequestModelMeta();

    /**
     * @return the singleton
     */
    public static JobRequestModelMeta get() {
       return slim3_singleton;
    }

    /** */
    public JobRequestModelMeta() {
        super("JobRequestModel", JobOcean.model.JobRequestModel.class);
    }

    @Override
    public JobOcean.model.JobRequestModel entityToModel(com.google.appengine.api.datastore.Entity entity) {
        JobOcean.model.JobRequestModel model = new JobOcean.model.JobRequestModel();
        model.setClosedDate((java.util.Date) entity.getProperty("closedDate"));
        model.setCompanyId((java.lang.Long) entity.getProperty("companyId"));
        model.setContracts(toList(java.lang.String.class, entity.getProperty("contracts")));
        model.setCreationDate((java.util.Date) entity.getProperty("creationDate"));
        model.setDescription((java.lang.String) entity.getProperty("description"));
        model.setEndDate((java.util.Date) entity.getProperty("endDate"));
        model.setId((java.lang.Long) entity.getProperty("id"));
        model.setJobRequestNumber((java.lang.String) entity.getProperty("jobRequestNumber"));
        model.setKey(entity.getKey());
        model.setModificationDate((java.util.Date) entity.getProperty("modificationDate"));
        model.setStartDate((java.util.Date) entity.getProperty("startDate"));
        model.setStatus((java.lang.String) entity.getProperty("status"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        JobOcean.model.JobRequestModel m = (JobOcean.model.JobRequestModel) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("closedDate", m.getClosedDate());
        entity.setProperty("companyId", m.getCompanyId());
        entity.setProperty("contracts", m.getContracts());
        entity.setProperty("creationDate", m.getCreationDate());
        entity.setProperty("description", m.getDescription());
        entity.setProperty("endDate", m.getEndDate());
        entity.setProperty("id", m.getId());
        entity.setProperty("jobRequestNumber", m.getJobRequestNumber());
        entity.setProperty("modificationDate", m.getModificationDate());
        entity.setProperty("startDate", m.getStartDate());
        entity.setProperty("status", m.getStatus());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        JobOcean.model.JobRequestModel m = (JobOcean.model.JobRequestModel) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        JobOcean.model.JobRequestModel m = (JobOcean.model.JobRequestModel) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        JobOcean.model.JobRequestModel m = (JobOcean.model.JobRequestModel) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        JobOcean.model.JobRequestModel m = (JobOcean.model.JobRequestModel) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
        JobOcean.model.JobRequestModel m = (JobOcean.model.JobRequestModel) model;
        m.setCreationDate(slim3_creationDateAttributeListener.prePut(m.getCreationDate()));
        m.setModificationDate(slim3_modificationDateAttributeListener.prePut(m.getModificationDate()));
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        JobOcean.model.JobRequestModel m = (JobOcean.model.JobRequestModel) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getClosedDate() != null){
            writer.setNextPropertyName("closedDate");
            encoder0.encode(writer, m.getClosedDate());
        }
        if(m.getCompanyId() != null){
            writer.setNextPropertyName("companyId");
            encoder0.encode(writer, m.getCompanyId());
        }
        if(m.getContracts() != null){
            writer.setNextPropertyName("contracts");
            writer.beginArray();
            for(java.lang.String v : m.getContracts()){
                encoder0.encode(writer, v);
            }
            writer.endArray();
        }
        if(m.getCreationDate() != null){
            writer.setNextPropertyName("creationDate");
            encoder0.encode(writer, m.getCreationDate());
        }
        if(m.getDescription() != null){
            writer.setNextPropertyName("description");
            encoder0.encode(writer, m.getDescription());
        }
        if(m.getEndDate() != null){
            writer.setNextPropertyName("endDate");
            encoder0.encode(writer, m.getEndDate());
        }
        if(m.getId() != null){
            writer.setNextPropertyName("id");
            encoder0.encode(writer, m.getId());
        }
        if(m.getJobRequestNumber() != null){
            writer.setNextPropertyName("jobRequestNumber");
            encoder0.encode(writer, m.getJobRequestNumber());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getModificationDate() != null){
            writer.setNextPropertyName("modificationDate");
            encoder0.encode(writer, m.getModificationDate());
        }
        if(m.getStartDate() != null){
            writer.setNextPropertyName("startDate");
            encoder0.encode(writer, m.getStartDate());
        }
        if(m.getStatus() != null){
            writer.setNextPropertyName("status");
            encoder0.encode(writer, m.getStatus());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected JobOcean.model.JobRequestModel jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        JobOcean.model.JobRequestModel m = new JobOcean.model.JobRequestModel();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("closedDate");
        m.setClosedDate(decoder0.decode(reader, m.getClosedDate()));
        reader = rootReader.newObjectReader("companyId");
        m.setCompanyId(decoder0.decode(reader, m.getCompanyId()));
        reader = rootReader.newObjectReader("contracts");
        {
            java.util.ArrayList<java.lang.String> elements = new java.util.ArrayList<java.lang.String>();
            org.slim3.datastore.json.JsonArrayReader r = rootReader.newArrayReader("contracts");
            if(r != null){
                reader = r;
                int n = r.length();
                for(int i = 0; i < n; i++){
                    r.setIndex(i);
                    java.lang.String v = decoder0.decode(reader, (java.lang.String)null)                    ;
                    if(v != null){
                        elements.add(v);
                    }
                }
                m.setContracts(elements);
            }
        }
        reader = rootReader.newObjectReader("creationDate");
        m.setCreationDate(decoder0.decode(reader, m.getCreationDate()));
        reader = rootReader.newObjectReader("description");
        m.setDescription(decoder0.decode(reader, m.getDescription()));
        reader = rootReader.newObjectReader("endDate");
        m.setEndDate(decoder0.decode(reader, m.getEndDate()));
        reader = rootReader.newObjectReader("id");
        m.setId(decoder0.decode(reader, m.getId()));
        reader = rootReader.newObjectReader("jobRequestNumber");
        m.setJobRequestNumber(decoder0.decode(reader, m.getJobRequestNumber()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("modificationDate");
        m.setModificationDate(decoder0.decode(reader, m.getModificationDate()));
        reader = rootReader.newObjectReader("startDate");
        m.setStartDate(decoder0.decode(reader, m.getStartDate()));
        reader = rootReader.newObjectReader("status");
        m.setStatus(decoder0.decode(reader, m.getStatus()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}